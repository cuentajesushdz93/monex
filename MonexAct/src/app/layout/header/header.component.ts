import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  @Input() title!: string;
  @Input() subtitle!: string;
  menuBtn: boolean = true;
  exitBtn: boolean = true;
  constructor() { }

  ngOnInit(): void {
  }

  public imprimir() {
    this.menuBtn = false;
    this.exitBtn = false;
    window.print();
    this.menuBtn = true;
    this.exitBtn = true;
  }

  public back() {
    history.back();
  }
}