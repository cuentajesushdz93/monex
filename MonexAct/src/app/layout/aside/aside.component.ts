import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

@Component({
  selector: 'app-aside',
  templateUrl: './aside.component.html',
  styleUrls: ['./aside.component.css']
})
export class AsideComponent implements OnInit {
  @ViewChild('collapseMovimiento') collapseMovimiento!: ElementRef;
  @ViewChild('collapseParticipante') collapseParticipante!: ElementRef;

  constructor() { }

  ngOnInit(): void { }

  toggleCollapse(): void {
    this.collapseMovimiento.nativeElement.classList.toggle('show');
  }

  toggleCollapseParticipante(): void {
    this.collapseParticipante.nativeElement.classList.toggle('show');
  }
}