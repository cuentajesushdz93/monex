import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { NavigationStart, Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
  @ViewChild('collapseElement') collapseElement!: ElementRef;
  title = 'MonexAct';
  headLogin: boolean = false;
  headApp: boolean = false;

  constructor(private router: Router) { }

  ngOnInit(): void {
    this.router.events.forEach((event) => {
      if (event instanceof NavigationStart) {
        if (event['url'] == '/' || event['url'] == '/login' || event['url'] == '/solicitud-contrasenia') {
          this.headLogin = true;
          this.headApp = false;
        } else {
          this.headLogin = false;
          this.headApp = true;
        }
      }
    });
  }

  toggleCollapse(): void {
    this.collapseElement.nativeElement.classList.toggle('show');
  }
}