import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule, Routes } from '@angular/router';

/* Components */
import { AppComponent } from './app.component';
import { HeaderLoginComponent } from './components/login/header/header.component';
import { HeaderComponent } from './layout/header/header.component';
import { LoginComponentComponent } from './components/login/login-component/login-component.component';
import { SolicitudContraseniaComponent } from './components/login/solicitud-contrasenia/solicitud-contrasenia.component';
import { InicioComponent } from './components/inicio/inicio.component';
import { DetalleNominaComponent } from './components/detalle-nomina/detalle-nomina.component';
import { DetalleEmpresaComponent } from './components/detalle-empresa/detalle-empresa.component';
import { AsideComponent } from './layout/aside/aside.component';
import { ListadoMovimientosComponent } from './components/movimientos/listado-movimientos/listado-movimientos.component';
import { HistorialMovimientosComponent } from './components/movimientos/historial-movimientos/historial-movimientos.component';
import { ParticipantesComponent } from './components/participantes/participantes.component';
import { DetalleEmpleadoComponent } from './components/detalle-empleado/detalle-empleado.component';
import { EstadoCuentaComponent } from './components/estado-cuenta/estado-cuenta.component';
import { MensualComponent } from './components/mensual/mensual.component';
import { ParticipantesComponent as Participantes } from './components/reportes/participantes/participantes.component';
import { SolicitudPrestamoComponent } from './components/reportes/solicitud-prestamo/solicitud-prestamo.component';
import { SolicitudRetiroComponent } from './components/reportes/solicitud-retiro/solicitud-retiro.component';
import { AccesoComponent } from './components/reportes/acceso/acceso.component';
import { CartaLiquidacionComponent } from './components/reportes/carta-liquidacion/carta-liquidacion.component';
import { PrestamosComponent } from './components/reportes/prestamos/prestamos.component';
import { DatosParticipantesComponent } from './components/reportes/datos-participantes/datos-participantes.component';

const routes: Routes = [
  { path: '', redirectTo: '/login', pathMatch: 'full' },
  { path: 'login', component: LoginComponentComponent },
  { path: 'solicitud-contrasenia', component: SolicitudContraseniaComponent },
  { path: 'inicio', component: InicioComponent },
  { path: 'detalle-nomina', component: DetalleNominaComponent },
  { path: 'detalle-empresa', component: DetalleEmpresaComponent },
  { path: 'listado-movimientos', component: ListadoMovimientosComponent },
  { path: 'historial-movimientos', component: HistorialMovimientosComponent },
  { path: 'participantes', component: ParticipantesComponent },
  { path: 'detalle-empleado', component: DetalleEmpleadoComponent },
  { path: 'configurar-estado-cuenta', component: EstadoCuentaComponent },
  { path: 'reporte-mensual-empresa', component: MensualComponent },
  { path: 'reporte-general-listado-participantes', component: Participantes },
  { path: 'solicitud-prestamo', component: SolicitudPrestamoComponent },
  { path: 'solicitud-retiro', component: SolicitudRetiroComponent },
  { path: 'de-acceso', component: AccesoComponent },
  { path: 'configurar-carta-liquidacion', component: CartaLiquidacionComponent },
  { path: 'listado-prestamos', component: PrestamosComponent },
  { path: 'generar-reporte-dato-empleados', component: DatosParticipantesComponent }
];

@NgModule({
  declarations: [
    AppComponent,
    HeaderLoginComponent,
    HeaderComponent,
    LoginComponentComponent,
    SolicitudContraseniaComponent,
    InicioComponent,
    DetalleNominaComponent,
    DetalleEmpresaComponent,
    AsideComponent,
    ListadoMovimientosComponent,
    HistorialMovimientosComponent,
    ParticipantesComponent,
    DetalleEmpleadoComponent,
    EstadoCuentaComponent,
    MensualComponent,
    Participantes,
    SolicitudPrestamoComponent,
    SolicitudRetiroComponent,
    AccesoComponent,
    CartaLiquidacionComponent,
    PrestamosComponent,
    DatosParticipantesComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot( routes )
    ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }