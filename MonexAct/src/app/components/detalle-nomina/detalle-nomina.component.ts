import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detalle-nomina',
  templateUrl: './detalle-nomina.component.html',
  styleUrls: ['./detalle-nomina.component.css']
})
export class DetalleNominaComponent implements OnInit {
  idNomina:  any = null;
  plan: string = "";
  permisos: number = 0;
  nombre : string = "";
  constructor(private route: ActivatedRoute) { }

  ngOnInit() :void{
    this.route.queryParams.subscribe(params => {
      this.idNomina = params['idNomina'];
      this.plan = params['plan'];
      this.permisos = params['permisos'];
      this.nombre = params['nombre'];
      }
    );
  }

}
