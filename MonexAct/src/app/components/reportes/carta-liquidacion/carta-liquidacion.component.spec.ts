import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CartaLiquidacionComponent } from './carta-liquidacion.component';

describe('CartaLiquidacionComponent', () => {
  let component: CartaLiquidacionComponent;
  let fixture: ComponentFixture<CartaLiquidacionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CartaLiquidacionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CartaLiquidacionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
