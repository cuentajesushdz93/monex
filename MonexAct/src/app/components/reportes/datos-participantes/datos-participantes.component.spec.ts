import { ComponentFixture, TestBed } from '@angular/core/testing';

import { DatosParticipantesComponent } from './datos-participantes.component';

describe('DatosParticipantesComponent', () => {
  let component: DatosParticipantesComponent;
  let fixture: ComponentFixture<DatosParticipantesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ DatosParticipantesComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(DatosParticipantesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
