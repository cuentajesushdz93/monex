import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detalle-empresa',
  templateUrl: './detalle-empresa.component.html',
  styleUrls: ['./detalle-empresa.component.css']
})
export class DetalleEmpresaComponent implements OnInit {
  plan: string = "";
  permisos: number = 0;
  nombre : string = "";
  constructor(private route: ActivatedRoute) { }

  ngOnInit() :void{
    this.route.queryParams.subscribe(params => {
      this.plan = params['plan'];
      this.permisos = params['permisos'];
      this.nombre = params['nombre'];
      }
    );
  }

}
