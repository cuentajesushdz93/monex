import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-detalle-empleado',
  templateUrl: './detalle-empleado.component.html',
  styleUrls: ['./detalle-empleado.component.css']
})

export class DetalleEmpleadoComponent implements OnInit {
  cveemp!: number
  idNomina!: number

  constructor(private route: ActivatedRoute) { }

  ngOnInit(): void { 
    this.route.queryParams.subscribe(params => {
      this.cveemp = params['cveemp'];
      this.idNomina = params['idNomina'];
    })
  }
}