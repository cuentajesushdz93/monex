import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SolicitudContraseniaComponent } from './solicitud-contrasenia.component';

describe('SolicitudContraseniaComponent', () => {
  let component: SolicitudContraseniaComponent;
  let fixture: ComponentFixture<SolicitudContraseniaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SolicitudContraseniaComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(SolicitudContraseniaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
